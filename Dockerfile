FROM python:2.7
MAINTAINER Dor Cohen
ADD . /usr/src/app
WORKDIR /usr/src/app
COPY requirements.txt ./
RUN pip install --cache-dir=.pip -r requirements.txt
RUN python manage.py collectstatic --noinput
EXPOSE 8000
CMD exec gunicorn verv.wsgi:application --log-level debug --bind 0.0.0.0:8000 --worker-class=gevent --worker-connections=1000 --workers=5