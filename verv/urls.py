import json
from django.conf.urls import url, include
from django.contrib import admin
from django.http import HttpResponse

def handler400(request):
    return HttpResponse(json.dumps({"detail": "Bad request."}), content_type="application/json", status=400)

def handler403(request):
    return HttpResponse(json.dumps({"detail": "You do not have permission to perform this action."}), content_type="application/json", status=403)

def handler404(request):
    return HttpResponse(json.dumps({"detail": "Not found."}), content_type="application/json", status=404)

def handler500(request):
    return HttpResponse(json.dumps({"detail": "Internal server error."}), content_type="application/json", status=500)

urlpatterns = [
    url(r'^admin/', include(admin.site.urls)),
    url(r'^app/', include('account.urls')),
    url(r'^mldata/', include('mldata.urls')),
    url(r'^api/v1/', include('plug.urls_api_v1')),
    url(r'^api/v1/', include('hub.urls_api_v1')),
    url(r'^api/v1/', include('account.urls_api_v1')),
]
