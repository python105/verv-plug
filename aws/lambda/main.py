"""Runs the lambda function systems for analysing plug data."""


from dynamo_analytics import DynamoAnalytics
from high_speed import HighSpeedData
from handlers import Handler
from setup import set_from_config


BUCKET = "v-plug-data-dev"


def run(event, context):
    """Acts as the handler for aws lambda.

    Args:
        event (dict): The event which triggers the lambda func
        context (dict): The context passed via aws when triggered
    """
    settings = set_from_config("plug") # TODO: Get the aws profile name
    dynamodb = DynamoAnalytics(settings=settings)

    filename = event['Records'][0]['s3']['object']['key']
    hsd = HighSpeedData(BUCKET, filename)

    power, duration = hsd.power, hsd.duration
    handler = Handler(event, context, power, duration, dynamodb)
    response = handler.process()
    return response
