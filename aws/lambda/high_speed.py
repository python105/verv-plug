"""Module to supply functions to analyse high speed data files."""

import numpy as np
import boto3


class HighSpeedData():
    """Class to calibrate and compute required data"""

    _client = boto3.client("s3")

    def __init__(self, bucket, filename):
        self._bucket = bucket
        self._filename = filename

        # constants
        self.sampling_rate = 10000
        self.n_adc = 4096
        self.voltage_scale = 670 / 3000
        self.current_scale = 0.0153
        self.calibration_scale = np.array([self.current_scale,
                                           self.voltage_scale])

        self._data = None
        self._duration = None
        self._power = None

    @property
    def data(self):
        """Gets the calibrated data."""
        if self._data is None:
            self._data = self.calibrate_data(self.load_binary_bucket())
        return self._data

    @property
    def duration(self):
        """Gets the duration."""
        if not self._duration:
            self._duration = self.get_duration(self.data)
        return self._duration

    @property
    def power(self):
        """Gets the average real power of the session."""
        if not self._power:
            self._power = self.average_real_power(self.data)
        return self._power

    @classmethod
    def load_from_buffer(cls, file_handler):
        """Loads a high speed data file from a buffer into a numpy array.

        Args:
            file_handler (Object): The object of which to read the file

        Returns:
            np.array: The loaded data file in np.array format
        """
        return np.frombuffer(file_handler.read(), dtype="<H").reshape(-1, 2).T

    def load_binary_bucket(self):
        """Loads the s3 file from the given bucket.

        Args:
            bucket (str): The bucket of which to load a file from
            filename (str): The name of the file to load

        Returns:
            np.ndarray: numpy array containing high speed plug data
        """
        file_handler = self._client.get_object(Bucket=self._bucket, Key=self._filename)["Body"]
        return self.load_from_buffer(file_handler)

    def calibrate_data(self, data: np.ndarray):
        """Calibrate the current and voltage from ADC counts.

        Args:
            data (np.ndarray): The high speed data of which to calibrate

        Returns:
            np.ndarray: The calibrated data
        """
        zero_means_data = abs(data - data.mean(axis=1, keepdims=True))
        return zero_means_data * self.calibration_scale[:, np.newaxis]

    @classmethod
    def average_real_power(cls, data: np.ndarray):
        """Calculates the average power for this calibrated data.

        Args:
            data (np.ndarray): The data from which to calculate the average power

        Returns:
            float: The average real power
        """
        return np.mean(data[0] * data[1])

    @classmethod
    def evaluate_rms(cls, data: np.ndarray):
        """Evaluates the rms for a given vector.

        Args:
            data (np.ndarray): The data from which to calculate the rms value

        Returns:
            float: The rms of the given vector
        """
        if len(data.shape) != 1:
            raise ValueError("This only works on vector data")
        zero_means_data = data - data.mean()
        return np.sqrt(np.mean(np.square(zero_means_data)))

    def get_duration(self, data: np.ndarray):
        """Calculates the duration of the high speed data file.

        Args:
            data (np.ndarray): The data from which to calculate the rms value
        """
        n_samples = data.shape[-1]
        return n_samples / self.sampling_rate
