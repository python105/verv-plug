"""Modules to configure the lambda functions to dynamo db."""


from collections import namedtuple
import copy
from decimal import Decimal

import boto3
from boto3.dynamodb.conditions import Key


ItemField = namedtuple("ItemField", ["name", "DTP"])

class DynamoAnalytics():
    """
    Configure dynamodb with lambda functions.
    """
    def __init__(self, precision=3, settings=None):
        self.precision = precision
        self.settings = settings
        self._client = None
        self._table = None

        self.item_fields = [ItemField("serial", "S"),
                            ItemField("usage", "N"),
                            ItemField("plug_usage", "N"),
                            ItemField("energy", "N"),
                            ItemField("consumption", "N"),
                            ItemField("start_timestamp", "N"),
                            ItemField("duration", "N")]

        self.increment_fields = ["energy", "duration", "consumption"]
        self.no_update_fields = ["start_timestamp"]

        self.reserved_kwargs = {"duration", "usage", "type"}
        self.reserved_map = {item.name: "#{}".format(item.name)
                             for item in self.item_fields if item.name in self.reserved_kwargs}

        self.type_mapping = {
            "energy": Decimal,
            "duration": Decimal,
            "consumption": Decimal
        }


    @property
    def client(self):
        """Gets the dynamo db client if it exists else, initialises it.

        Returns:
            client: The aws client for dynamodb
        """
        if not self._client:
            self._connect()
        return self._client

    @property
    def table(self):
        """Gets the dynamo db table if it exists else, initialises it.

        Returns:
            table: The aws client for dynamodb
        """
        if not self._table:
            self._connect()
        return self._table

    def _connect(self):
        """Connects the database."""
        client = boto3.resource("dynamodb", **self.settings)

        if client:
            self._client = client

        table = self.client.Table("v-plug-analytics")
        if table:
            self._table = table

    def get_last_usage_entry(self, serial):
        """Gets the most recent usage id, returns both the plug id and the calc id."""
        projection_expression = ", ".join(
            self.reserved_map.get(item.name, item.name) for item in self.item_fields)

        response = self.table.query(
            ProjectionExpression=projection_expression,
            ExpressionAttributeNames={v: k for k, v in self.reserved_map.items()},
            Limit=1,
            ScanIndexForward=False,
            KeyConditionExpression=Key("serial").eq(serial)
        )

        return response["Items"][0]

    @classmethod
    def new_id(cls, last_id=0):
        """Generate a new usage id."""
        return last_id + 1

    def _prepare_decimal_string(self, val: float) -> str:
        """Provides a string formatted floating point for conversion to a decimal."""
        return ("{:.0" + str(self.precision) + "f}").format(val)

    def _convert_kwarg(self, item_name, **kwargs):
        """Converts the kwarg value to a string representation."""
        value = kwargs[item_name]
        try:
            mapper = self.type_mapping[item_name]
        except KeyError:
            return value
        else:
            if mapper == Decimal:
                return mapper(self._prepare_decimal_string(value))
            return mapper(value)

    def new_usage_info(self, **kwargs):
        """Creates a new entry for a usage period."""
        usage_info = {}
        for item in self.item_fields:
            usage_info[item.name] = self._convert_kwarg(item.name, **kwargs)
        return usage_info

    def update_usage_info(self, existing_info: dict, **kwargs):
        """Updates an existing entry with new usage information."""
        new_info = copy.copy(existing_info)
        for item in self.item_fields:
            if item.name in self.increment_fields:
                new_info[item.name] = existing_info[item.name] + \
                                      self._convert_kwarg(item.name, **kwargs)
            elif item.name in self.no_update_fields:
                pass
            elif kwargs[item.name] != existing_info[item.name]:
                raise ValueError("Mismatch between existing and new data")

            return new_info

    def save_usage_info(self, usage_info: dict):
        """Adds the current information to the existing usage.

        Creates a new instance if the usage ID doesn't exist
        This is overwrite items in the DB if the value already exists
        """
        self.table.put_item(Item=usage_info)
