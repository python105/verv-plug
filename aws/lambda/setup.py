"""AWS Lambda setup."""


import configparser
import os


def set_from_config(
        config_name=None,
        region="eu-west-2",
        config_path=os.path.join(os.getcwd(), ".aws", "credentials")
):
    """Setups up the lambda instances."""
    if not config_name:
        config_name = "default"
    config = configparser.ConfigParser()

    config.read(config_path)
    aws_access_key_id = config.get(config_name, "aws_access_key_id")
    aws_secret_access_key = config.get(config_name, "aws_secret_access_key")
    aws_session_token = config.get(config_name, "aws_session_token")

    settings = {
        "region_name": region,
        "aws_access_key_id": aws_access_key_id,
        "aws_secret_access_key": aws_secret_access_key,
        "aws_session_token": aws_session_token,
    }

    return settings
