"""Helpers for AWS Lambda and high speed data file."""


import calendar
import collections
import os


def timestamp_usage_from_filename(filepath: str):
    """Gets the unix timestamp and usage count from a file.

    Args:
        filepath (str): The filepath to get the timestamp from

    Returns:
        tuple: format:(<timestamp>, <usage>)
    """
    filename = os.path.basename(filepath)
    unpacked = collections.deque(reversed(filename.split("-")))
    fields = ("serial", "year", "month", "day", "hour", "minute", "second", "usage")
    mapping = collections.OrderedDict()

    for field in fields:
        try:
            value = unpacked.pop()
        except IndexError:
            mapping[field] = 0
        else:
            if field == "year":
                if len(value) != 4:
                    unpacked.appendleft(value)
                    value = unpacked.pop()
            mapping[field] = value

    _, *timestamp_fields, usage = fields
    timestamp = calendar.timegm([int(mapping[field]) for field in timestamp_fields])

    return timestamp, mapping[usage]


def serial_from_filename(filepath: str):
    """Gets the serial number from the filepath.

    Args:
        filepath (str): The filepath to get the serial number from

    Returns:
        str: serial number
    """
    return filepath.split("/")[0]
