"""Handler class to manage lambda function internals."""


import json

from helpers import timestamp_usage_from_filename, serial_from_filename


class Handler():
    """Handler class to for dynamodb.

    Args:
        event (dict): Dictionary formatted event response from aws
        context (dict): Dictionary formatted event meta data from aws
        power (float): The power calculated from the s3 bucket file
        duration (float): The timed length of the s3 bucket file
    """
    def __init__(self, event, context, power, duration, dynamo_analytics):
        self.event = event
        self.context = context
        self.power = power
        self.duration = duration
        self.dynamo_analytics = dynamo_analytics

    def process(self):
        """Processes the event that triggers the lambda function."""

        filename = self.event['Records'][0]['s3']['object']['key']

        timestamp, plug_usage = timestamp_usage_from_filename(filename)
        serial = serial_from_filename(filename)

        method = {
            "create": self.dynamo_analytics.new_usage_info,
            "update": self.dynamo_analytics.update_usage_info,
        }

        kwargs = {
            "serial": serial,
            "plug_usage": plug_usage,
            "energy": self.power * self.duration,
            "consumption": self.power,
            "duration": self.duration,
            "start_timestamp": timestamp,
        }

        try:
            usage_info = self.dynamo_analytics.get_last_usage_entry(serial)
        except IndexError:
            usage_id = self.dynamo_analytics.new_id()
            usage_info = self._get_or_create(method["create"], usage_id, kwargs)
        else:
            if usage_info["plug_usage"] != plug_usage:
                usage_id = self.dynamo_analytics.new_id(usage_info["usage"])
                usage_info = self._get_or_create(method["create"], usage_id, kwargs)
            else:
                usage_id = usage_info["usage"]
                usage_info = self._get_or_create(method["update"], usage_id, kwargs, usage_info)

        self.dynamo_analytics.save_usage_info(usage_info)
        return json.dumps({"serial": serial, "usage": int(usage_id)})

    @classmethod
    def _get_or_create(cls, method, usage, kwargs, *args):
        """Gets or creates an new usage instance depending on the method passed.

        Args:
            method (func): The dynamodb method to use
            usage  (Decimal): The usage as calculated from the filename
            kwargs (dict): The keyword arguments to pass
        """
        kwargs.update({"usage": usage})
        return method(*args, **kwargs)
