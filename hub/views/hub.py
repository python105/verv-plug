# -*- coding: utf-8 -*-
from ..models import Hub
from ..serializers.hub import HubSerializer

from django.conf import settings

from rest_framework import status
from rest_framework.permissions import IsAdminUser
from rest_framework.views import APIView
from rest_framework.generics import CreateAPIView,UpdateAPIView, ListAPIView, RetrieveAPIView, DestroyAPIView
from rest_framework.response import Response

import boto3
import json

class Register(CreateAPIView):
	permission_classes = [IsAdminUser]
	serializer_class = HubSerializer

	def create(self, request, *args, **kwargs):

		db_response = super(CreateAPIView, self).create(request, *args, **kwargs)

		iot = boto3.client('iot',
			aws_access_key_id=settings.AWS_ACCESS_KEY_ID,
			aws_secret_access_key=settings.AWS_SECRET_ACCESS_KEY,
			region_name=settings.AWS_REGION_NAME
		)

		try :
			group = iot.create_thing_group(
				thingGroupName=request.data.get("group"),
				thingGroupProperties={
					'thingGroupDescription': request.data.get("group_description")
				}
			)
		except Exception as error:
			try :
				group = iot.update_thing_group(
					thingGroupName=request.data.get("group"),
					thingGroupProperties={ 'thingGroupDescription': request.data.get("group_description") }
				)
			except Exception as error:
				group = error.message

		try :
			type = iot.create_thing_type(
				thingTypeName=request.data.get("type"),
				thingTypeProperties={
					'thingTypeDescription': request.data.get("type_description"),
					'searchableAttributes': ['serial_number','mac_address']
				}
			)
		except Exception as error:
			type = error.message

		try :	
			certificate = iot.create_keys_and_certificate(
				setAsActive=True
			)
		except Exception as error:
			certificate = error.message

		try: 
			thing = iot.create_thing(
				thingName=request.data.get("serial_number"),
				thingTypeName=request.data.get("type"),
				attributePayload={
					'attributes': {
						'serial_number': request.data.get("serial_number"),
						'mac_address':request.data.get("mac_address")
					}
				}
			)

			iot.add_thing_to_thing_group(
				thingGroupName=request.data.get("group"),
				thingName=request.data.get("serial_number")
			)
			iot.attach_thing_principal(
				principal=certificate['certificateArn'],
				thingName=request.data.get("serial_number")
			)
		except Exception as error:
			thing = error.message

		response = {'thing' : thing}
		response.update({'type' : type})
		response.update({'group' : group})
		response.update({'certificate' : certificate})

		return Response(response,status=status.HTTP_200_OK)

class Delete(DestroyAPIView):
	permission_classes = [IsAdminUser]
	queryset = Hub.objects.all()
	serializer_class = HubSerializer
	lookup_field = 'serial_number'

	def delete(self, request, *args, **kwargs):

		response = super(DestroyAPIView, self).destroy(request, *args, **kwargs)

		iot = boto3.client('iot',
			aws_access_key_id=settings.AWS_ACCESS_KEY_ID,
			aws_secret_access_key=settings.AWS_SECRET_ACCESS_KEY,
			region_name=settings.AWS_REGION_NAME
		)
		try:
			principals = iot.list_thing_principals( thingName=kwargs["serial_number"] )

			for principal in principals['principals']:
				certificateId = principal.split('/')[1] 
				iot.detach_thing_principal( thingName=kwargs["serial_number"], principal=principal )
				iot.update_certificate( certificateId=certificateId,newStatus='INACTIVE' )
				iot.delete_certificate( certificateId=certificateId, forceDelete=True )

			iot.delete_thing( thingName=kwargs["serial_number"] )

		except Exception as error:
			response = Response(error.message)

		return response
		


class Update(UpdateAPIView):
	permission_classes = [IsAdminUser]
	queryset = Hub.objects.all()
	serializer_class = HubSerializer
	lookup_field = 'serial_number'

class Assign(UpdateAPIView):
	serializer_class = HubSerializer
	lookup_field = 'serial_number'

	def get_queryset(self):
		return Hub.objects.filter(user=None).filter(activated=False)

	def update(self, request, *args, **kwargs):
		instance = self.get_object()
		instance.activated = True
		instance.user = request.user
		instance.save()

		serializer = self.get_serializer(instance,data={},partial=True)
		serializer.is_valid(raise_exception=True)
		self.perform_update(serializer)

		return Response(serializer.data)

class Unassign(UpdateAPIView):
	serializer_class = HubSerializer
	lookup_field = 'serial_number'

	def get_queryset(self):
		return Hub.objects.filter(user=self.request.user)

	def update(self, request, *args, **kwargs):
		instance = self.get_object()
		instance.activated = False
		instance.user = None
		instance.save()

		serializer = self.get_serializer(instance,data={},partial=True)
		serializer.is_valid(raise_exception=True)
		self.perform_update(serializer)

		return Response(serializer.data)

class List(ListAPIView):
	serializer_class = HubSerializer
	def get_queryset(self):
		return Hub.objects.filter(user=self.request.user)

class Show(RetrieveAPIView):
	serializer_class = HubSerializer
	lookup_field = 'serial_number'
	def get_queryset(self):
		return Hub.objects.filter(user=self.request.user)