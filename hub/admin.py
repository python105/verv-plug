# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.contrib import admin

from .models import Hub

@admin.register(Hub)
class HubAdmin(admin.ModelAdmin):
    list_display = ('user','serial_number','type','activated','release_version',)
    ordering = ('serial_number',)
    search_fields = ('serial_number',)