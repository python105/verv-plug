from django.conf.urls import url

from views import hub

urlpatterns = [	
	url(r'^hub/register/$', hub.Register.as_view(), name='hub register'),
	url(r'^hub/delete/(?P<serial_number>[-\w]+)/$', hub.Delete.as_view(), name='hub delete'),
	url(r'^hub/show/(?P<serial_number>[-\w]+)/$', hub.Show.as_view(), name='hub show'),
	url(r'^hub/assign/(?P<serial_number>[-\w]+)/$', hub.Assign.as_view(), name='hub assign'),
	url(r'^hub/unassign/(?P<serial_number>[-\w]+)/$', hub.Unassign.as_view(), name='hub unassign'),
	url(r'^hub/update/(?P<serial_number>[-\w]+)/$', hub.Update.as_view(), name='hub update'),
	url(r'^hub/list/$', hub.List.as_view(), name='hub list')
]