from rest_framework import serializers
from rest_framework import exceptions

from ..models import Hub

class HubSerializer(serializers.ModelSerializer):
    serial_number = serializers.RegexField(regex=r'^[-\w]+$', min_length=3, max_length=15, allow_blank=False)
    user = serializers.CharField(required=False,write_only=True)
    activated = serializers.BooleanField(write_only=True)
    type_description = serializers.CharField(write_only=True,required=False)
    type = serializers.ChoiceField(choices=('HUBV1','HUBV2',),write_only=True)
    group = serializers.ChoiceField(choices=('VERV','DOE','DEWA','PTT'),write_only=True)
    group_description = serializers.CharField(write_only=True,required=False)
    
    class Meta:
        model = Hub
        fields = ('user','serial_number','activated','type','type_description','release_version','mac_address','group','group_description')

    def create(self, validated_data):
        hub = Hub.objects.create(**validated_data)
        return hub

    def update(self, instance, validated_data):
        instance.user = validated_data.get('user', instance.user)
        instance.serial_number = validated_data.get('serial_number', instance.serial_number)
        instance.mac_address = validated_data.get('mac_address', instance.mac_address)
        instance.activated = validated_data.get('activated', instance.activated)
        instance.type = validated_data.get('type', instance.type)
        instance.type_description = validated_data.get('type_description', instance.type_description)
        instance.release_version = validated_data.get('release_version', instance.release_version)
        instance.group = validated_data.get('group', instance.group)
        instance.group_description = validated_data.get('group_description', instance.group_description)
        instance.save()
        return instance

    def validate(self, data):
        return data