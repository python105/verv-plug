# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models
from account.models import User

class Hub(models.Model):
	TYPE_TYPE = (
		('HUBV1', 'Hub V1 Prototype'),
		('HUBV2', 'Hub V2 Production'),
	)
	GROUP_TYPE = (
		('VERV', 'VERV'),
		('DOE', 'DOE'),
		('DEWA', 'DEWA'),
		('PTT', 'PTT'),
	)
	user = models.ForeignKey(User, on_delete=models.CASCADE, related_name='hub_user',blank=True,null=True)
	serial_number = models.CharField(max_length=45,unique=True)
	mac_address = models.CharField(max_length=45,unique=True)
	activated = models.BooleanField(default=True)
	type = models.CharField(max_length=55,choices=TYPE_TYPE,default='HUBV1',)
	type_description = models.TextField(default='')
	group = models.CharField(max_length=55,choices=GROUP_TYPE,default='VERV',)
	group_description = models.TextField(default='')
	release_version = models.CharField(max_length=55)

	def __str__(self):
		return self.serial_number