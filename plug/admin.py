# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.contrib import admin

from .models import Action, Activity, Appliance, Fault, Plug

@admin.register(Plug)
class PlugAdmin(admin.ModelAdmin):
    list_display = ('user','serial_number','type','activated','release_version',)
    ordering = ('serial_number',)
    search_fields = ('serial_number',)

@admin.register(Appliance)
class ApplianceAdmin(admin.ModelAdmin):
    list_display = ('user','plug','type','name','brand','model','age','purchase_price',)
    ordering = ('user',)
    search_fields = ('name','user','plug','model','brand')

@admin.register(Activity)
class ActivityAdmin(admin.ModelAdmin):
    list_display = ('start_time','end_time','plug','appliance','user',)
    ordering = ('start_time',)
    search_fields = ('start_time','user','plug',)

@admin.register(Action)
class ActionAdmin(admin.ModelAdmin):
    list_display = ('start_time','end_time','plug','appliance','user','description',)
    ordering = ('start_time',)
    search_fields = ('start_time','user','plug',)

@admin.register(Fault)
class FaultAdmin(admin.ModelAdmin):
    list_display = ('time', 'plug', 'appliance', 'user', 'description', 'name', 'type',)
    ordering = ('time',)
    search_fields = ('time', 'user', 'plug',)
