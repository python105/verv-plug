# -*- coding: utf-8 -*-
from __future__ import unicode_literals
from django.db import models
from account.models import User
from django.contrib.postgres.fields import JSONField

class Plug(models.Model):
	TYPE_TYPE = (
		('PLUGV1', 'Plug V1 Prototype'),
		('PLUGV2', 'Plug V2 Production'),
	)
	GROUP_TYPE = (
		('VERV', 'VERV'),
	)
	user = models.ForeignKey(User, on_delete=models.CASCADE, related_name='plug_user',blank=True,null=True)
	serial_number = models.CharField(max_length=45,unique=True)
	mac_address = models.CharField(max_length=45,unique=True)
	activated = models.BooleanField(default=True)
	type = models.CharField(max_length=55,choices=TYPE_TYPE,default='PLUGV1',)
	type_description = models.TextField(default='')
	group = models.CharField(max_length=55,choices=GROUP_TYPE,default='VERV',)
	group_description = models.TextField(default='')
	release_version = models.CharField(max_length=55)

	def __str__(self):
		return self.serial_number

class Appliance(models.Model):
	APPLIANCE_TYPE = (
		('DW', 'Dish Washer'),
		('TD', 'Tumble Dryer'),
		('HD', 'Hair Dryer'),
		('AC', 'Aircon'),
		('BL', 'Boiler'),
		('WM', 'Washing Maching'),
		('RF', 'Refrigerator'),
		('FR', 'Freezer')
	)
	user = models.ForeignKey(User, on_delete=models.CASCADE, related_name='appliance_user')
	plug = models.OneToOneField(Plug, on_delete=models.CASCADE, related_name='appliance_plug',blank=True,null=True)
	type = models.CharField(max_length=55,choices=APPLIANCE_TYPE,default='DW',)
	name = models.CharField(max_length=55)
	brand = models.CharField(max_length=55)
	serial = models.CharField(max_length=55,blank=True,null=True)
	model = models.CharField(max_length=55)
	age = models.IntegerField()
	purchase_price = models.FloatField()

	def __str__(self):
		return self.name

class Activity(models.Model):
	plug = models.ForeignKey(Plug, on_delete=models.CASCADE, related_name='activity_plug')
	appliance = models.ForeignKey(Appliance, on_delete=models.CASCADE, related_name='activity_appliance')
	user = models.ForeignKey(User, on_delete=models.CASCADE, related_name='activity_user')
	start_time = models.DateTimeField(auto_now_add=True)
	end_time = models.DateTimeField(auto_now_add=False,default=None,blank=True,null=True,)

class Action(models.Model):
	appliance = models.ForeignKey(Appliance, on_delete=models.CASCADE, related_name='action_appliance')
	user = models.ForeignKey(User, on_delete=models.CASCADE, related_name='action_user')
	plug = models.ForeignKey(Plug, on_delete=models.CASCADE, related_name='action_plug')
	description = JSONField(max_length=55)
	start_time = models.DateTimeField(auto_now_add=True)
	end_time = models.DateTimeField(auto_now_add=False,default=None,blank=True,null=True,)

	def get_duration(self):
		return ((self.end_time - self.start_time).seconds/60)

class Fault(models.Model):
	FAULT_TYPE = (
		('F1', 'Fault 1'),
		('F2', 'Fault 2'),
	)
	type = models.CharField(max_length=55,choices=FAULT_TYPE,default='F1',)
	appliance = models.ForeignKey(Appliance, on_delete=models.CASCADE, related_name='fault_appliance')
	user = models.ForeignKey(User, on_delete=models.CASCADE, related_name='fault_user')
	plug = models.ForeignKey(Plug, on_delete=models.CASCADE, related_name='fault_plug')
	name = models.CharField(max_length=55)
	description = JSONField(max_length=55)
	time = models.DateTimeField(auto_now_add=True)
