"""Views for the faults class."""


from datetime import datetime

from django.conf import settings
from rest_framework import status
from rest_framework.permissions import IsAdminUser
from rest_framework.views import APIView
from rest_framework.response import Response
from rest_framework.generics import (CreateAPIView,
                                     DestroyAPIView,
                                     ListAPIView,
                                     UpdateAPIView,
                                     RetrieveAPIView)

from ..models import Appliance, Fault, Plug
from ..serializers.fault import FaultSerializer


class Create(CreateAPIView):
    """View for creating a fault."""
    serializers_class = FaultSerializer

    def create(self, request, *args, **kwargs):
        """Overidding super method: view for creating a fault."""
        return super(CreateAPIView, self).create(request, *args,**kwargs)


class Delete(DestroyAPIView):
    """View for deleting the fault."""
    serializer_class = FaultSerializer
    lookup_field = 'id'

    def get_queryset(self):
        """Overides super method."""
        return Fault.objects.filter(user_id=self.request.user.id)


class Update(UpdateAPIView):
    """View for deleting the fault."""
    serializer_class = FaultSerializer
    lookup_field = 'id'

    def get_queryset(self):
        """Overides super method."""
        return Fault.objects.filter(user_id=self.request.user.id)


class List(ListAPIView):
    """Lists all faults created by a user."""
    serializer_class = FaultSerializer

    def get_queryset(self):
        """Query set to search from."""
        return Fault.objects.filter(user_id=self.request.user.id)


class Show(RetrieveAPIView):
    """Gets a specific fault determined via the pk passed"""
    serializer_class = FaultSerializer
    lookup_field = 'id'

    def get_queryset(self):
        """Query set to search from."""
        return Fault.objects.filter(user_id=self.request.user.id)


class Types(APIView):
    """Lists all faults of a given type."""
    def get(self, request):
        """Overidden super method to be called upon HTTP method `GET`."""
        return Response(settings.WIDGETS.get('faults','{}'), status=status.HTTP_200_OK)
