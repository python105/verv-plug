# -*- coding: utf-8 -*-
from ..models import Appliance, Plug, Activity
from ..serializers.appliance import ApplianceSerializer

from rest_framework import status
from rest_framework.permissions import IsAdminUser
from rest_framework.views import APIView
from rest_framework.generics import CreateAPIView,UpdateAPIView, ListAPIView, RetrieveAPIView, DestroyAPIView
from rest_framework.response import Response

from datetime import datetime

class Create(CreateAPIView):
	serializer_class = ApplianceSerializer

	def create(self, request, *args, **kwargs):
		if Appliance.objects.filter(user=request.user).count() < 10:
			return super(CreateAPIView, self).create(request, *args, **kwargs)
		else:
			return Response({'detail':'Forbidden : Max appliances.'},status=403)

class Delete(DestroyAPIView):
	serializer_class = ApplianceSerializer
	lookup_field = 'id'
	def get_queryset(self):
		return Appliance.objects.filter(user=self.request.user)

class Update(UpdateAPIView):
	serializer_class = ApplianceSerializer
	lookup_field = 'id'
	def get_queryset(self):
		return Appliance.objects.filter(user=self.request.user)

class Assign(UpdateAPIView):
	serializer_class = ApplianceSerializer
	lookup_field = 'id'

	def get_queryset(self):
		return Appliance.objects.filter(user=self.request.user).filter(plug=None)

	def update(self, request, *args, **kwargs):
		plug_sn = request.data.get("plug")
		plug = Plug.objects.filter(user=request.user).filter(serial_number=plug_sn).first()
		if plug:
			instance = self.get_object()
			
			instance.plug = plug
			instance.save()

			serializer = self.get_serializer(instance,data={},partial=True)
			serializer.is_valid(raise_exception=True)
			self.perform_update(serializer)

			activity = Activity()
			activity.appliance = instance
			activity.plug = plug
			activity.user = request.user
			activity.save()

			return Response(serializer.data)
		else :
			return Response({'detail':'Forbidden : Plug not found.'},status=403)

class Unassign(UpdateAPIView):
	serializer_class = ApplianceSerializer
	lookup_field = 'id'

	def get_queryset(self):
		return Appliance.objects.filter(user_id=self.request.user.id).exclude(plug=None)

	def update(self, request, *args, **kwargs):
		instance = self.get_object()
		
		instance.plug = None
		instance.save()

		serializer = self.get_serializer(instance,data={},partial=True)
		serializer.is_valid(raise_exception=True)
		self.perform_update(serializer)

		activity = Activity.objects.filter(user=request.user).filter(appliance=instance).filter(end_time=None).last()
		activity.end_time = datetime.now()
		activity.save()

		return Response(serializer.data)

class List(ListAPIView):
    serializer_class = ApplianceSerializer
    def get_queryset(self):
		return Appliance.objects.filter(user_id=self.request.user.id)

class Show(RetrieveAPIView):
    serializer_class = ApplianceSerializer
    lookup_field = 'id'
    def get_queryset(self):
		return Appliance.objects.filter(user_id=self.request.user.id)