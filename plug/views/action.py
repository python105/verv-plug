# -*- coding: utf-8 -*-
from ..models import Appliance, Plug, Action
from ..serializers.action import ActionSerializer

from rest_framework import status
from rest_framework.views import APIView
from rest_framework.generics import CreateAPIView,UpdateAPIView, ListAPIView, RetrieveAPIView, DestroyAPIView
from rest_framework.response import Response

from django.conf import settings

from datetime import datetime

class Start(CreateAPIView):
	serializer_class = ActionSerializer
	lookup_field = 'appliance'

	def create(self, request, *args, **kwargs):

		appliance_id = request.data.get("appliance_id")
		appliance = Appliance.objects.filter(user=request.user).filter(id=appliance_id).exclude(plug_id=None).first()
		action = Action.objects.filter(appliance=appliance).filter(end_time=None).first()
		if appliance and not action:
			return super(CreateAPIView, self).create(request, *args, **kwargs)
		elif action:
			return Response({'detail':'Forbidden : Please Stop previous action.'},status=status.HTTP_403_FORBIDDEN)
		else:
			return Response({'detail':'Forbidden : Wrong Appliance.'},status=status.HTTP_403_FORBIDDEN)

class Stop(UpdateAPIView):
	serializer_class = ActionSerializer

	def get_object(self):
		return Action.objects.filter(user=self.request.user).filter(end_time=None).filter(appliance=self.request.data.get('appliance_id')).exclude(plug_id=None).first()

	def update(self, request, *args, **kwargs):
		instance = self.get_object()
		if instance:
			instance.end_time = datetime.now()

			serializer = self.get_serializer(instance,data={},partial=True)
			serializer.is_valid(raise_exception=True)
			self.perform_update(serializer)

			return Response(serializer.data)
		else:
			return Response({'detail':'Not found.'},status=status.HTTP_404_NOT_FOUND)

class Update(UpdateAPIView):
	serializer_class = ActionSerializer
	lookup_field = 'id'
	def get_queryset(self):
		return Action.objects.filter(user_id=self.request.user.id)

	def update(self, request, *args, **kwargs):
		instance = self.get_object()
		instance.description = request.data.get("description")

		serializer = self.get_serializer(instance,data={},partial=True)
		serializer.is_valid(raise_exception=True)
		self.perform_update(serializer)

		return Response(serializer.data)

class List(ListAPIView):
    serializer_class = ActionSerializer
    def get_queryset(self):
		running_flag = self.request.GET.get('running')
		action = Action.objects.filter(user_id=self.request.user.id)
		if running_flag == 'true':
			return action.filter(end_time=None)
		elif running_flag == 'false':
			return action.exclude(end_time=None)
		else:
			return action

class Show(RetrieveAPIView):
    serializer_class = ActionSerializer
    lookup_field = 'id'
    def get_queryset(self):
		return Action.objects.filter(user_id=self.request.user.id)

class Types(APIView):
    def get(self, request):
		return Response(settings.WIDGETS.get('actions', '{}'), status=status.HTTP_200_OK )
