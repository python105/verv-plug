# -*- coding: utf-8 -*-
from ..models import Activity
from ..serializers.activity import ActivitySerializer

from rest_framework.generics import ListAPIView, RetrieveAPIView

from datetime import datetime

class List(ListAPIView):
    serializer_class = ActivitySerializer
    def get_queryset(self):
		return Activity.objects.filter(user=self.request.user)

class Show(RetrieveAPIView):
    serializer_class = ActivitySerializer
    lookup_field = 'id'
    def get_queryset(self):
		return Activity.objects.filter(user=self.request.user)