from django.conf.urls import url

from views import plug, appliance, activity, action, fault

urlpatterns = [	
	url(r'^plug/register/$', plug.Register.as_view(), name='plug register'),
	url(r'^plug/delete/(?P<serial_number>[-\w]+)/$', plug.Delete.as_view(), name='plug delete'),
	url(r'^plug/show/(?P<serial_number>[-\w]+)/$', plug.Show.as_view(), name='plug show'),
	url(r'^plug/assign/(?P<serial_number>[-\w]+)/$', plug.Assign.as_view(), name='plug assign'),
	url(r'^plug/unassign/(?P<serial_number>[-\w]+)/$', plug.Unassign.as_view(), name='plug unassign'),
	url(r'^plug/update/(?P<serial_number>[-\w]+)/$', plug.Update.as_view(), name='plug update'),
	url(r'^plug/list/$', plug.List.as_view(), name='plug list'),
	
	url(r'^appliance/create/$', appliance.Create.as_view(), name='appliance create'),
	url(r'^appliance/delete/(?P<id>[-\w\s]+)/$', appliance.Delete.as_view(), name='appliance delete'),
	url(r'^appliance/show/(?P<id>[-\w\s]+)/$', appliance.Show.as_view(), name='appliance show'),
	url(r'^appliance/assign/(?P<id>[-\w\s]+)/$', appliance.Assign.as_view(), name='appliance assign'),
	url(r'^appliance/unassign/(?P<id>[-\w\s]+)/$', appliance.Unassign.as_view(), name='appliance unassign'),
	url(r'^appliance/update/(?P<id>[-\w\s]+)/$', appliance.Update.as_view(), name='appliance update'),
	url(r'^appliance/list/$', appliance.List.as_view(), name='appliance list'),

	url(r'^activity/list/$', activity.List.as_view(), name='activity list'),
	url(r'^activity/show/(?P<id>[-\w\s]+)/$', activity.Show.as_view(), name='activity show'),

	url(r'^action/start/$', action.Start.as_view(), name='action start'),
	url(r'^action/stop/$', action.Stop.as_view(), name='action stop'),
	url(r'^action/update/(?P<id>[-\w\s]+)/$', action.Update.as_view(), name='action update'),
	url(r'^action/list/$', action.List.as_view(), name='action list'),
    url(r'^action/show/(?P<id>[-\w\s]+)/$', action.Show.as_view(), name='action show'),
	url(r'^action/types/list/$', action.Types.as_view(), name='action types list'),


    url(r'^faults/create/$', fault.Create.as_view(), name='fault create'),
    url(r'^faults/delete/(?P<id>[-\w\s]+)/$', fault.Delete.as_view(), name='fault delete'),
    url(r'^faults/update/(?P<id>[-\w\s]+)/$', fault.Update.as_view(), name='fault update'),
    url(r'^faults/list/$', fault.List.as_view(), name='fault list'),
    url(r'^faults/show/(?P<id>[-\w\s]+)/$', fault.Show.as_view(), name='fault show'),
    url(r'^faults/types/list/$', fault.Types.as_view(), name='fault types list')
]
