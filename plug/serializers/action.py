from rest_framework import serializers
from rest_framework import exceptions
from appliance import ApplianceSerializer

from ..models import Action, Appliance, Plug

class ActionSerializer(serializers.ModelSerializer):
	plug = serializers.CharField(required=False,write_only=True)
	appliance_id = serializers.CharField(required=True,write_only=True)
	appliance = ApplianceSerializer(read_only=True)
	description = serializers.JSONField(required=False,)
	start_time = serializers.DateTimeField(required=False)
	end_time = serializers.DateTimeField(required=False)

	class Meta:
		model = Action
		fields = ('id','plug','appliance_id','description','start_time','end_time','appliance')

	def create(self, validated_data):
		appliance = Appliance.objects.get(id=validated_data['appliance_id'],user=self.context['request'].user)
		validated_data['user'] = self.context['request'].user
		validated_data['appliance'] = appliance
		validated_data['plug'] = appliance.plug
		action = Action.objects.create(**validated_data)
		return action

	def update(self, instance, validated_data):
		instance.plug = validated_data.get('plug', instance.plug)
		instance.appliance = validated_data.get('appliance_id', instance.appliance)
		instance.description = validated_data.get('description', instance.description)
		instance.start_time = validated_data.get('start_time', instance.start_time)
		instance.end_time = validated_data.get('end_time', instance.end_time)
		instance.save()
		return instance

	def validate(self, data):
		return data