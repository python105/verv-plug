from rest_framework import serializers
from rest_framework import exceptions

from ..models import Appliance, Plug

class ApplianceSerializer(serializers.ModelSerializer):
    user = serializers.CharField(required=False,write_only=True)
    plug = serializers.CharField(required=False)
    brand = serializers.CharField(required=True)
    serial = serializers.CharField(required=False)
    type = serializers.ChoiceField(choices=('DW','TD','HD','AC','BL','WM','RF','FR'),required=True)
    age = serializers.IntegerField()
    purchase_price = serializers.IntegerField()

    class Meta:
        model = Appliance
        fields = ('id','user','serial','brand','plug','type','name','model','age','purchase_price')

    def create(self, validated_data):
        validated_data['user'] = self.context['request'].user
        plug_sn = self.context['request'].data.get('plug')
        if plug_sn:
            plug = Plug.objects.filter(user=self.context['request'].user).filter(serial_number=plug_sn).first()
            appliance = Appliance.objects.filter(plug=plug)
            if plug and not appliance:
                validated_data['plug'] = plug
            else:
                del validated_data['plug'] 
        appliance = Appliance.objects.create(**validated_data)
        return appliance

    def update(self, instance, validated_data):
        instance.type = validated_data.get('type', instance.type)
        instance.name = validated_data.get('name', instance.name)
        instance.brand = validated_data.get('brand', instance.brand)
        instance.serial = validated_data.get('serial', instance.serial)
        instance.name = validated_data.get('name', instance.name)
        instance.model = validated_data.get('model', instance.model)
        instance.age = validated_data.get('age', instance.age)
        instance.purchase_price = validated_data.get('purchase_price', instance.purchase_price)
        instance.save()
        return instance

    def validate(self, data):
        return data