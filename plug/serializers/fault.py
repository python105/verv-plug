"""Serializer for the faults model."""


from rest_framework import serializers

from ..models import Appliance, Fault
from ..serializers.appliance import ApplianceSerializer


class FaultSerializer(serializers.ModelSerializer):
    """Serializer class for Faults."""
    plug = serializers.CharField(required=False, write_only=True)
    name = serializers.CharField(required=True, max_length=155)
    type = serializers.JSONField()
    appliance_id = serializers.CharField()
    appliance = ApplianceSerializer(read_only=True)
    time =  serializers.DateTimeField(required=True)
    description = serializers.CharField(max_length=55)

    class Meta:
        model = Fault
        fields = (
            'id',
            'name',
            'plug',
            'appliance_id',
            'description',
            'type',
            'appliance',
            'time',
        )

    def create(self, validated_data):
        """Creates a new instance of a fault."""
        _id = validated_data["appliance_id"]
        user = self.context['request'].user
        appliance = Appliance.objects.get(id=_id, user=user)
        validated_data['user'] = user
        validated_data['appliance'] = appliance
        validated_data['plug'] = plug
        fault = Fault.create(**validated_data)
        return fault

    def update(self, instance, validated_data):
        """Updates an already existing serialized fault."""
        instance.plug = validated_data.get("plug", None)
        instance.name = validated_data.get("name", None)
        instance.type = validated_data.get("type", None)
        instance.appliance_id = validated_data.get("appliance_id", None)
        instance.appliance = validated_data.get("appliance", None)
        instance.time = validated_data.get("time", None)
        instance.description = validated_data.get("description", None)
        instance.save()
        return instance

