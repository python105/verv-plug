from rest_framework import serializers
from rest_framework import exceptions

from ..models import Activity

class ActivitySerializer(serializers.ModelSerializer):
	appliance = serializers.CharField()
	plug = serializers.CharField()
	start_time = serializers.DateTimeField(required=False)
	end_time = serializers.DateTimeField(required=False)

	class Meta:
		model = Activity
		fields = ('plug','appliance','start_time','end_time')

	def create(self, validated_data):
		validated_data['user'] = self.context['request'].user
		activity = Activity.objects.create(**validated_data)
		return activity

	def update(self, instance, validated_data):
		instance.user = validated_data.get('user', instance.user)
		instance.plug = validated_data.get('plug', instance.plug)
		instance.appliance = validated_data.get('appliance', instance.appliance)
		instance.start_time = validated_data.get('start_time', instance.start_time)
		instance.end_time = validated_data.get('end_time', instance.end_time)
		instance.save()
		return instance

	def validate(self, data):
		return data