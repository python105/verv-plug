from rest_framework import serializers
from rest_framework import exceptions
from rest_framework.authtoken.models import Token
from rest_framework.validators import UniqueValidator

from ..models import User

class AccountSerializer(serializers.ModelSerializer):
    password = serializers.CharField(write_only=True)
    email = serializers.EmailField(validators=[UniqueValidator(queryset=User.objects.all(),message='A user with that email already exists.')])
    backup_email = serializers.EmailField(validators=[UniqueValidator(queryset=User.objects.all(),message='A user with that email already exists.')],required=False)
    
    class Meta:
        model = User
        fields = ('email','first_name','middle_name', 'last_name', 'backup_email', 'address', 'postcode', 'password',)

    def create(self, validated_data):
        if 'backup_email' not in validated_data:
            validated_data['backup_email'] = validated_data['email']
        user = User.objects.create(**validated_data)
        user.set_password(validated_data['password'])
        user.save()
        Token.objects.create(user=user)
        return user

    def update(self, instance, validated_data):
        instance.first_name = validated_data.get('first_name', instance.first_name)
        instance.first_name = validated_data.get('middle_name', instance.first_name)
        instance.last_name = validated_data.get('last_name', instance.last_name)
        instance.address = validated_data.get('address', instance.address)
        instance.postcode = validated_data.get('postcode', instance.postcode)
        instance.backup_email = validated_data.get('backup_email', instance.backup_email)
        if 'password' in validated_data:
            instance.set_password(validated_data['password'])
        instance.save()
        return instance

    def validate(self, data):
        return data