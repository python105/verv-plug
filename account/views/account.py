from ..models import User
from ..serializers.account import AccountSerializer

from django.contrib.auth import authenticate, login, logout
from django.utils.crypto import get_random_string
from django.core.mail import send_mail
from django.urls import reverse
from django.conf import settings
from django.shortcuts import render, redirect
from django.utils.http import urlsafe_base64_encode, urlsafe_base64_decode   
from django.http import HttpResponse

from rest_framework import status
from rest_framework.permissions import AllowAny
from rest_framework.views import APIView
from rest_framework.generics import CreateAPIView,UpdateAPIView,RetrieveAPIView,ListAPIView,DestroyAPIView
from rest_framework.response import Response
from rest_framework.authtoken.models import Token
from rest_framework.authentication import TokenAuthentication

import urllib
import urllib2
import json

TokenAuthentication.keyword = 'Bearer'

class Create(CreateAPIView):
	permission_classes = [AllowAny]
	serializer_class = AccountSerializer

	def create(self, request, *args, **kwargs):

		# Begin reCAPTCHA validation
		recaptcha_response = request.data.get('captcha')
		url = 'https://www.google.com/recaptcha/api/siteverify'
		values = {
			'secret': settings.GOOGLE_RECAPTCHA_SECRET_KEY,
			'response': recaptcha_response
		}
		data = urllib.urlencode(values)
		req = urllib2.Request(url, data)
		response = urllib2.urlopen(req)
		result = json.load(response)
		# End reCAPTCHA validation
		if result['success'] or True:
			 
			response = super(CreateAPIView, self).create(request, *args, **kwargs)
			instance = User.objects.filter(email=request.data.get("email")).first()

			token = urlsafe_base64_encode(str(Token.objects.filter(user=instance).first().key))
			host = request.get_host()
			uri = reverse('account verify', args=(instance.email,token))
			link = "http://%s%s" % (host,uri)

			send_mail(
				"Verv : Verify your account",
				"Please click the following link to verify your account :\n\n%s" % (link),
				settings.DEFAULT_FROM_EMAIL,
				[instance.email],
				html_message = "Please click the following link to verify your account :<br/><a href='%s'>Verify Account</a>" % (link),
				fail_silently=False,
			)

			return response
		else:
			return Response({'detail':'Wrong Captcha.'},status=status.HTTP_403_FORBIDDEN)
class Update(UpdateAPIView):
	serializer_class = AccountSerializer
	queryset = User.objects.all()

	def get_object(self):
		queryset = self.filter_queryset(self.get_queryset())
		obj = queryset.get(pk=self.request.user.id)
		return obj

	def update(self, request, *args, **kwargs):
		serializer = self.get_serializer(self.get_object(),data=request.data,partial=True)
		serializer.is_valid(raise_exception=True)
		self.perform_update(serializer)

		return Response(serializer.data)

class Show(RetrieveAPIView):
	serializer_class = AccountSerializer
	queryset = User.objects.all()
	
	def get_object(self):
		queryset = self.filter_queryset(self.get_queryset())
		obj = queryset.get(pk=self.request.user.id)
		return obj

class Verify(RetrieveAPIView):
	permission_classes = [AllowAny]
	serializer_class = AccountSerializer
	queryset = User.objects.all()
	lookup_field = 'email'
	
	def get(self, request, *args, **kwargs):
		token = urlsafe_base64_decode(kwargs['token'])
		instance = self.get_object()
		if str(Token.objects.filter(user=instance).first().key) == str(token):
			instance.is_active = True
			instance.save()
			Token.objects.filter(user=instance).delete()
			Token.objects.update_or_create(user=instance)
			serializer = AccountSerializer(instance, many=False)
			return Response({'detail' : 'User is verified', 'user' : serializer.data},status=status.HTTP_200_OK)
		else:
			return Response(status=status.HTTP_403_FORBIDDEN)

class ForgotPassowrd(RetrieveAPIView):
	permission_classes = [AllowAny]
	serializer_class = AccountSerializer
	queryset = User.objects.all()
	lookup_field = 'email'
	
	def get(self, request, *args, **kwargs):
		instance = self.get_object()

		token = urlsafe_base64_encode(str(Token.objects.filter(user=instance).first().key))
		host = request.get_host()
		uri = reverse('account reset password', args=(instance.email,token))
		link = "http://%s%s" % (host,uri)

		send_mail(
			"Verv : Reset password instructions",
			"Please click the following link to reset your password :\n\n%s" % (link),
			settings.DEFAULT_FROM_EMAIL,
			[instance.email],
			html_message = "Please click the following link to reset your password :<br/><a href='%s'>Reset Password</a>" % (link),
			fail_silently=False,
		)

		return Response({'detail': 'Reset password instructions were sent to your email address.'},status=status.HTTP_200_OK)

class ResetPassowrd(RetrieveAPIView):
	permission_classes = [AllowAny]
	serializer_class = AccountSerializer
	queryset = User.objects.all()
	lookup_field = 'email'
	
	def get(self, request, *args, **kwargs):
		token = urlsafe_base64_decode(kwargs['token'])
		instance = self.get_object()
		if str(Token.objects.filter(user=instance).first().key) == str(token):
			link = "verv://resetPassword?email=%s&token=%s" % (instance.email,str(token))
			response = HttpResponse("", status=302)
			response['Location'] = link
			return response
		else:
			return Response(status=status.HTTP_403_FORBIDDEN)

class SetOTP(UpdateAPIView):
	permission_classes = [AllowAny]
	serializer_class = AccountSerializer
	queryset = User.objects.all()
	lookup_field = 'email'
	
	def update(self, request, *args, **kwargs):
		token = urlsafe_base64_decode(kwargs['token'])
		instance = self.get_object()
		if str(Token.objects.filter(user=instance).first().key) == str(token):
			otp = get_random_string(12, allowed_chars='ABCDEFGHIabcdefghi0123456789')
			instance.set_password(str(otp))
			instance.save()
			Token.objects.filter(user=instance).delete()
			Token.objects.update_or_create(user=instance)
			return Response({'OTP':otp},status=status.HTTP_200_OK)
		else:
			return Response({'message':urlsafe_base64_encode(kwargs['token'])},status=status.HTTP_403_FORBIDDEN)

class Disable(APIView):

	def put(self, request):
		instance = request.user
		instance.is_active = False
		instance.save()
		Token.objects.filter(user=request.user).delete()
		return Response(status=status.HTTP_202_ACCEPTED)

class Restore(RetrieveAPIView):
	permission_classes = [AllowAny]
	serializer_class = AccountSerializer
	queryset = User.objects.all()
	lookup_field = 'email'

	def get(self, request, *args, **kwargs):

		instance = self.get_object()
		
		if instance.is_active == False:

			obj,created = Token.objects.update_or_create(user=instance)
			token = urlsafe_base64_encode(str(obj.key))
			
			host = request.get_host()
			uri = reverse('account verify', args=(instance.email,token))
			link = "http://%s%s" % (host,uri)

			send_mail(
				"Verv : Restore your account",
				"Please click the following link to restore your account :\n\n%s" % (link),
				settings.DEFAULT_FROM_EMAIL,
				[instance.email],
				html_message = "Please click the following link to restore your account :<br/><a href='%s'>Restore Account</a>" % (link),
				fail_silently=False,
			)
			return Response({'detail': 'Restore Account instructions were sent to your email address.'},status=status.HTTP_200_OK)
		
		else:
			return Response(status=status.HTTP_401_UNAUTHORIZED)

		return response

class Login(APIView):
	permission_classes = [AllowAny]
	def post(self, request,):
		email = request.data.get("email")
		password = request.data.get("password")
		user = authenticate(email=email, password=password)
		if user:
			if user.is_active:
				'''
				login(request, user)				
				serializer = AccountSerializer(user)
				'''
				Token.objects.filter(user=user).delete()
				Token.objects.update_or_create(user=user)
				serializer = AccountSerializer(user,data={},partial=True)
				serializer.is_valid(raise_exception=True)
				response={'token': user.auth_token.key}
				response.update(serializer.data)
				return Response( response ,status=status.HTTP_200_OK )
			else:
				return Response({'detail' : 'User is not verified'} ,status=status.HTTP_403_FORBIDDEN)
		else:
			return Response({'detail' : 'User is not authorized'},status=status.HTTP_401_UNAUTHORIZED)

class Logout(APIView):
	
	def get(self, request):
		'''
		logout(request)
		'''
		Token.objects.filter(user=request.user).delete()
		Token.objects.update_or_create(user=request.user)
		return Response({'detail' : 'Logout successful'},status=status.HTTP_202_ACCEPTED)