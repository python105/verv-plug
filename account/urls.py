from django.conf.urls import url

from views import account

urlpatterns = [
	url(r'^account/verify/(?P<email>[\w\.-]+@[\w\.-]+(\.[\w]+)+)/token/(?P<token>[-\w]+)/$', account.Verify.as_view(), name='account verify'),
	url(r'^account/resetpassword/(?P<email>[\w\.-]+@[\w\.-]+(\.[\w]+)+)/token/(?P<token>[-\w]+)/$', account.ResetPassowrd.as_view(), name='account reset password'),
]