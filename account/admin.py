# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.contrib import admin

from .models import User

@admin.register(User)
class UserAdmin(admin.ModelAdmin):
    list_display = ('email','backup_email','first_name','middle_name','last_name','address','is_active','is_staff')
    ordering = ('email',)
    search_fields = ('first_name','last_name','first_name','email','backup_email')
