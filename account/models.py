# -*- coding: utf-8 -*-
from __future__ import unicode_literals
from django.contrib.auth.models import AbstractUser
from django.db import models

class User(AbstractUser):
	CURRENCY_TYPE = (
		('GBP', 'Great British Pound'),
		('ILS', 'Israel Shekel'),
	)
	first_name = models.CharField(max_length=16)
	middle_name = models.CharField(max_length=16,blank=True,null=True)
	last_name = models.CharField(max_length=16)
	email = models.CharField(max_length=64, unique=True)
	backup_email = models.CharField(max_length=64, unique=True)
	postcode = models.CharField(max_length=16)
	address = models.CharField(max_length=512)
	energy_tariff = models.FloatField(blank=True,null=True)
	currency = models.CharField(max_length=8,choices=CURRENCY_TYPE,default='GBP',)
	mpan = models.CharField(max_length=128,blank=True,null=True)
	is_staff = models.BooleanField(default=False)
	is_active = models.BooleanField(default=False)
	username = None

	USERNAME_FIELD = 'email'
	REQUIRED_FIELDS = ['first_name', 'last_name', 'postcode', 'address']

	def __str__(self):
		return self.email
	
	def get_full_name(self):
		return "%s %s" % (self.first_name,self.last_name)
