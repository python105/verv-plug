from django.conf.urls import url

from views import account

urlpatterns = [
	url(r'^account/login/$', account.Login.as_view(), name='account login'),
	url(r'^account/logout/$', account.Logout.as_view(), name='account logout'),
	url(r'^account/create/$', account.Create.as_view(), name='account create'),
	url(r'^account/setotp/(?P<email>[\w\.-]+@[\w\.-]+(\.[\w]+)+)/token/(?P<token>[-\w]+)/$', account.SetOTP.as_view(), name='account setotp'),
	url(r'^account/forgotpassword/(?P<email>[\w\.-]+@[\w\.-]+(\.[\w]+)+)/$', account.ForgotPassowrd.as_view(), name='account forgot password'),
	url(r'^account/restore/(?P<email>[\w\.-]+@[\w\.-]+(\.[\w]+)+)/$', account.Restore.as_view(), name='account restore'),
	url(r'^account/show/$', account.Show.as_view(), name='account show'),
	url(r'^account/update/$', account.Update.as_view(), name='account update'),
	url(r'^account/disable/$', account.Disable.as_view(), name='account disable'),
]