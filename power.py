#!/usr/bin/env python
import numpy as np
import matplotlib.pyplot as plt
import sys

def main(filename):
    np.set_printoptions(threshold=sys.maxsize)
    with open(filename, 'rb') as fin:
        data = np.frombuffer(fin.read(), "<H")
    data = data.reshape(-1, 2)
    duration = []
    voltage = []
    energy = []
    #data = calibrate(data)
    for sample, line in enumerate(data):
        print("{} - {}, {}".format(sample, line[0], line[1]))
        if sample < 500:
            duration.append(sample)
            voltage.append(line[0])
            energy.append(line[1])
    
    # Show scatter points graph
    #plt.scatter(duration, voltage, s=5)
    # Show a single line graph
    plt.plot(duration, voltage, linewidth=1)
    plt.plot(duration, energy, linewidth=1)
    # Set chart title.
    plt.title("Power", fontsize=19)
    # Set x axis label.
    plt.xlabel("Duration", fontsize=10)
    # Set y axis label.
    plt.ylabel("Voltage/Energy", fontsize=10)
    # Set size of tick labels.
    plt.tick_params(axis='both', which='major', labelsize=9)
    # Display the plot in the matplotlib's viewer.
    plt.show()

def calibrate(data):
    # Scale is V or A per ADC count
    VOlly done
    CALIBRATION_SCALE = np.array([VOLTAGE_SCALE, CURRENT_SCALE])
LTAGE_SCALE = 670 / 3000  # Float division by default; welcome to python3!
    CURRENT_SCALE = 0.0153 # manua
    data = data - data.mean(axis=1, keepdims=True) # convert to zero means
    data[0] = abs(data[0]) # enforce positivety since the above calculation is perfectly accurate
    data[1] = abs(data[1]) # ^
    return (data) * CALIBRATION_SCALE[:, np.newaxis]

if __name__ == "__main__":
    main(sys.argv[1])