from django.conf.urls import url

from views import main
from django.contrib.auth import views as auth_views

urlpatterns = [
	url(r'^$', main.results, name='main page'),
	url(r'^index/$', main.results, name='results page'),
	url(r'^register/$', main.register, name='register page'),
	url(r'^forgot/$', main.forgot, name='forgot password page'),
	url(r'^change-password/(?P<email>[\w\.-]+@[\w\.-]+(\.[\w]+)+)/token/(?P<token>[-\w]+)/$', main.changepassword, name='change password page'),
	url(r'^verify-account/(?P<email>[\w\.-]+@[\w\.-]+(\.[\w]+)+)/token/(?P<token>[-\w]+)/$', main.verify, name='verify account page'),
	url(r'^login/$', auth_views.login,{'template_name': 'login.html'}, name='login page'),
	url(r'^logout/$', auth_views.logout, {'next_page': '/mldata/login/'}, name='logout page'),
]