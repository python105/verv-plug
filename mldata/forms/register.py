from django import forms
from django.contrib.auth.forms import UserCreationForm
from django.apps import apps
from rest_framework.validators import UniqueValidator

User = apps.get_model('account','User')

class RegisterForm(UserCreationForm):
    password1 = forms.CharField(max_length=30)
    email = forms.EmailField(max_length=254,help_text='Required. Inform a valid email address.',)
    first_name = forms.CharField(max_length=30,required=True, help_text='Required a valid first name.')
    last_name = forms.CharField(max_length=30,required=True)
    middle_name = forms.CharField(max_length=30,required=False)
    backup_email = forms.EmailField(max_length=254,help_text='Required. Inform a valid email address.',required=False)
    postcode = forms.CharField(max_length=30,required=False)
    address = forms.CharField(max_length=60,required=False)
    password2 = None

    class Meta:
        model = User
        fields = ('email','first_name','middle_name', 'last_name', 'backup_email', 'address', 'postcode', 'password1',)