# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.shortcuts import render, redirect
from django.http import HttpResponse
from django.apps import apps
from django.urls import reverse
from django.utils.crypto import get_random_string
from django.db.models import Count, Q
from django.core.paginator import Paginator, EmptyPage, PageNotAnInteger
from django.contrib.auth.decorators import login_required
from django.contrib.auth import login, authenticate
from django.contrib import messages
from django.core.mail import send_mail
from django.conf import settings
from django.utils.http import urlsafe_base64_encode, urlsafe_base64_decode
from rest_framework.authtoken.models import Token
from rest_framework.authentication import TokenAuthentication

import urllib
import urllib2
import json

from ..forms.register import RegisterForm


Plug = apps.get_model('plug','Plug')
Action = apps.get_model('plug','Action')
Appliance = apps.get_model('plug','Appliance')
Account = apps.get_model('account','User')

@login_required(login_url='/mldata/login/')
def results(request):
    brand = request.GET.get('brand')
    type = request.GET.get('type')
    age = request.GET.get('age')
    query = request.GET.get('q')

    actions_list = Action.objects.all().exclude(end_time=None)
    plugs = Plug.objects.filter(user=request.user)
    menu = Appliance.objects.distinct('brand').exclude(plug=None)
    if brand:
        actions_list = actions_list.filter(appliance__brand=brand)
    if type:
        actions_list = actions_list.filter(appliance__type=type)
    if age:
        actions_list = actions_list.filter(appliance__age__range=(int(age)-12,int(age)))
    if query:
        actions_list = actions_list.filter( Q(appliance__brand__icontains=str(query)) | Q(appliance__type__icontains=str(query)) | Q(appliance__model__icontains=str(query)) | Q(description__icontains=str(query)) )

    page = request.GET.get('page', 1)
    paginator = Paginator(actions_list, 6)

    try:
        actions = paginator.page(page)
    except PageNotAnInteger:
        actions = paginator.page(1)
    except EmptyPage:
        actions = paginator.page(paginator.num_pages)

    return render(request, 'results.html', {'user' : request.user, 'actions' : actions, 'menu' : menu})

def register(request):
    if request.method == 'POST':
        updated_request = request.POST.copy()
        updated_request.update({'backup_email': request.POST.get('email')})
        updated_request.update({'address': 'None'})
        updated_request.update({'postcode': 'None'})
        form = RegisterForm(updated_request)
        if form.is_valid():

            # Begin reCAPTCHA validation
            recaptcha_response = updated_request.get('g-recaptcha-response')
            url = 'https://www.google.com/recaptcha/api/siteverify'
            values = {
                'secret': settings.GOOGLE_RECAPTCHA_WEB_SECRET_KEY,
                'response': recaptcha_response
            }
            data = urllib.urlencode(values)
            req = urllib2.Request(url, data)
            response = urllib2.urlopen(req)
            result = json.load(response)
            # End reCAPTCHA validation
            
            if result['success']:
                instance = form.save()
                Token.objects.create(user=instance)
                token = urlsafe_base64_encode(str(Token.objects.filter(user=instance).first().key))
                host = request.get_host()
                uri = reverse('verify account page', args=(instance.email,token))
                link = "http://%s%s" % (host,uri)

                send_mail(
                    "Verv : Verify your account",
                    "Please click the following link to verify your account :\n\n%s" % (link),
                    settings.DEFAULT_FROM_EMAIL,
                    [instance.email],
                    html_message = "Please click the following link to verify your account :<br/><a href='%s'>Verify Account</a>" % (link),
                    fail_silently=False,
                )

                messages.success(request, 'Please Activate your account')
    else:
        form = RegisterForm()
    return render(request, 'register.html', {'form': form})

def forgot(request):
    if request.method == 'POST':
        user = Account.objects.filter(email=request.POST.get('email')).first()
        if user:

            token = urlsafe_base64_encode(str(Token.objects.filter(user=user).first().key))
            host = request.get_host()
            uri = reverse('change password page', args=(user.email,token))
            link = "http://%s%s" % (host,uri)

            send_mail(
                "Verv : Access to your account",
                "Please use the following link to reset your account password :\n\n%s" % (link),
                settings.DEFAULT_FROM_EMAIL,
                [user.email],
                html_message = "Please use the following link to reset your account password  :<br/><b>%s</b>" % (link),
                fail_silently=False,
            )
            messages.success(request, 'Please Look in your Email box')
    return render(request, 'forgot.html')

def changepassword(request,email,token):
    if request.method == 'POST':
        token = urlsafe_base64_decode(token)
        user = Account.objects.filter(email=email).first()
        if user and str(Token.objects.filter(user=user).first().key) == str(token):
            user.set_password(str(request.POST.get('password1')))
            user.save()
            messages.success(request, 'Password has changed')
            Token.objects.filter(user=user).delete()
            Token.objects.update_or_create(user=user)
    return render(request, 'changepassword.html')

def verify(request,email,token):
    if request.method == 'GET':
        token = urlsafe_base64_decode(token)
        user = Account.objects.filter(email=email).first()
        if user and str(Token.objects.filter(user=user).first().key) == str(token):
            user.is_active = True
            user.save()
            messages.success(request, 'Account is verified')
            Token.objects.filter(user=user).delete()
            Token.objects.update_or_create(user=user)
    return render(request, 'login.html')